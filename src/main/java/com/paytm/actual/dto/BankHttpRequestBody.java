package com.paytm.actual.dto;


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONObject;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


public class BankHttpRequestBody {

	private MultiValueMap<String, Object> requestBody;
	private String requestBody1;
	private ObjectMapper mapper = new ObjectMapper();


	public BankHttpRequestBody() {
		this.requestBody = new LinkedMultiValueMap<String, Object>();
	}

	public BankHttpRequestBody(MultiValueMap<String, Object> requestBody) {
		this.requestBody = requestBody;
	}
	
	public BankHttpRequestBody(JsonNode obj) {
		this.requestBody = convertObjectToMap(obj);
	}
	
	public BankHttpRequestBody(String obj) {
		
		this.requestBody1 = obj;
	}

	

	/**
	 * @return the requestBody
	 */
	public MultiValueMap<String, Object> getRequestBody() {
		return requestBody;
	}
	
	public String getRequestBodyAsString() {
		return requestBody1;
	}
	
	
	
	/**
	 * @param requestBody the requestBody to set
	 */
	public void setRequestBody(MultiValueMap<String, Object> requestBody) {
		this.requestBody = requestBody;
	}
	public void setRequestBodyAsString(String requestBody) {
		this.requestBody1 = requestBody;
	}
	
	
     public Map<String, Object> convertIntoMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		Iterator<String> it = this.requestBody.keySet().iterator();
		while (it.hasNext()) {
			String theKey = (String) it.next();
			parameters.put(theKey, this.requestBody.getFirst(theKey));
		}

		return parameters;
	}
	
	public JsonNode convertIntoJsonNode(){
		
		JsonNode node = mapper.valueToTree(this.requestBody);
		return node;
	}
	
	public JSONObject convertIntoJsonObject(){
		JSONObject json = new JSONObject(this.requestBody);
		return json;
	}
	
	public Map<String, String> convertMultiToRegularMap() {
		MultiValueMap<String, Object> m = requestBody;
	    Map<String, String> map = new HashMap<String, String>();
	    if (m == null) {
	        return map;
	    }
	    for (Entry<String, List<Object>> entry : m.entrySet()) {
	        StringBuilder sb = new StringBuilder();
	        for (Object s : entry.getValue()) {
	            if (sb.length() > 0) {
	                sb.append(',');
	            }
	            sb.append(s.toString());
	        }
	        map.put(entry.getKey(), sb.toString());
	    }
	    return map;
	}
	
	public MultiValueMap<String, Object> convertObjectToMap(JsonNode obj){
		MultiValueMap<String, Object> map = new LinkedMultiValueMap();
		map.add("json", obj);
		//mapper.convertValue(obj, LinkedMultiValueMap.class);
		return map;
	}
	public MultiValueMap<String, Object> convertStringToMap(String obj){
		MultiValueMap<String, Object> map = new LinkedMultiValueMap();
		map.add("key",obj);
		//mapper.convertValue(obj, LinkedMultiValueMap.class);
		return map;
	}
	
}
