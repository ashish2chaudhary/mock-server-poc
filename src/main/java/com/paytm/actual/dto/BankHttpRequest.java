package com.paytm.actual.dto;


import org.springframework.http.HttpMethod;


public class BankHttpRequest {

	private String url;
	private HttpMethod method; 
	private BankHttpRequestHeaders requestHeaders;
	private BankHttpRequestBody requestBody;

	
	public BankHttpRequest() {
	}
	

	public BankHttpRequest(String url, HttpMethod method, BankHttpRequestHeaders requestHeaders,
			BankHttpRequestBody requestBody) {
		this.url = url;
		this.method = method;
		this.requestHeaders = requestHeaders;
		this.requestBody = requestBody;
	}
	


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BankHttpRequest [url=" + url + ", method=" + method + ", requestHeaders=" + requestHeaders
				+ ", requestBody=" + requestBody + "]";
	}


	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the method
	 */
	public HttpMethod getMethod() {
		return method;
	}
	/**
	 * @param method the method to set
	 */
	public void setMethod(HttpMethod method) {
		this.method = method;
	}
	/**
	 * @return the requestHeaders
	 */
	public BankHttpRequestHeaders getRequestHeaders() {
		return requestHeaders;
	}
	/**
	 * @param requestHeaders the requestHeaders to set
	 */
	public void setRequestHeaders(BankHttpRequestHeaders requestHeaders) {
		this.requestHeaders = requestHeaders;
	}
	/**
	 * @return the requestBody
	 */
	public BankHttpRequestBody getRequestBody() {
		return requestBody;
	}
	
	/**
	 * @param requestBody the requestBody to set
	 */
	public void setRequestBody(BankHttpRequestBody requestBody) {
		this.requestBody = requestBody;
	}
	
	
	
}

