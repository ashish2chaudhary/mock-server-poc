package com.paytm.actual.dto;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.http.HttpHeaders;


public class BankHttpRequestHeaders {

	private HttpHeaders requestHeaders;

	public BankHttpRequestHeaders() {
		this.requestHeaders = new HttpHeaders();
	}

	public BankHttpRequestHeaders(Map requestHeaders) {
		this.requestHeaders = new HttpHeaders();
		Iterator itr = requestHeaders.entrySet().iterator();
		while(itr.hasNext()){
			Map.Entry<String,String> entry = (Entry<String, String>) itr.next();
			this.requestHeaders.add(entry.getKey(), entry.getValue());
		}
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.requestHeaders.toString();
	}

	/**
	 * @return the requestHeaders
	 */
	public HttpHeaders getRequestHeaders() {
		return requestHeaders;
	}

	/**
	 * @param requestHeaders the requestHeaders to set
	 */
	public void setRequestHeaders(Map requestHeaders) {
		Iterator itr = requestHeaders.entrySet().iterator();
		while(itr.hasNext()){
			Map.Entry<String,String> entry = (Entry<String, String>) itr.next();
			this.requestHeaders.add(entry.getKey(), entry.getValue());
		}
	}
	
	public void setNewHeader(String key, String value){
		this.requestHeaders.set(key, value);
	}
}
