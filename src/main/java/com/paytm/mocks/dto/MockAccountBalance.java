package com.paytm.mocks.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class MockAccountBalance {
	
    private String accountNumber="9999999999";
    protected String effectiveBalance="999999.99";
    protected String slfdBalance="0";
    protected String unclearBalance="0";
    private String status="success";
    private String responseCode="200";
    
    @JsonProperty("accountNumber")
    public String getAccountNumber() {
        return accountNumber;
    }

    public MockAccountBalance setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;;
        return this;
    }

    @JsonProperty("effectiveBalance")
    public String getEffectiveBalance() {
        return effectiveBalance;
    }

    public MockAccountBalance setEffectiveBalance(String effectiveBalance) {
        this.effectiveBalance = effectiveBalance;
        return this;
    }

    @JsonProperty("slfdBalance")
    public String getslfdBalance() {
        return slfdBalance;
    }

    public MockAccountBalance setslfdBalance(String slfdBalance) {
        this.slfdBalance = slfdBalance;
        return this;
    }

    @JsonProperty("unclearBalance")
    public String getUnclearBalance() {
        return unclearBalance;
    }

    public MockAccountBalance setUnclearBalance(String unclearBalance) {
        this.unclearBalance = unclearBalance;
        return this;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    public MockAccountBalance setstatus(String status) {
        this.status = status;
        return this;
    }

    @JsonProperty("responseCode")
    public String getResponseCode() {
        return responseCode;
    }

    public MockAccountBalance setResponseCode(String responseCode) {
        this.responseCode = responseCode;
        return this;
    }
    

    public String postitiveResponse() {
        return "{" +
                	"\"accounts\": [" + 
                		"{" +
                			"\"accountNumber\":" + "\"" + accountNumber + "\"," +
                			"\"effectiveBalance\":" + effectiveBalance + "," +
                			"\"slfdBalance\":" + slfdBalance + "," +
                			"\"unclearBalance\":" + unclearBalance +
                		"}" +
                	"]," +
                	"\"status\":" + "\"" + status + "\"," +
                	"\"response_code\":" + responseCode +
                "}";                
    }
    
    public String negativeResponse() {
        return "{" +
        			"\"status\":" + "\"" + "failure" + "\"," +
                	"\"message\":" + "\"" + "Account number is invalid" + "\"," +
                	"\"response_code\":" + "1139" + "," +
                	"\"txn_id\":" + "\"" + "A0I3U0MC000DR" + "\"" +
                "}";                
    }
}

