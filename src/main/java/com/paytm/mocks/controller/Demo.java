package com.paytm.mocks.controller;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

import org.codehaus.jackson.JsonProcessingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.paytm.actual.dto.BankHttpRequest;
import com.paytm.actual.dto.BankHttpRequestBody;
import com.paytm.actual.dto.BankHttpRequestHeaders;
import com.paytm.mocks.dto.MockAccountBalance;


@RestController()
@RequestMapping(path = "/demo")
public class Demo {
	
	@Autowired
	private RestTemplate restTemplate;
	
	private ObjectMapper mapper = new ObjectMapper();
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}
	
	private ResponseEntity<JsonNode> entityResponse; 
	
	private HttpStatus status;
	
	private JsonNode response;
	
	
	@RequestMapping(
			path = "/accountBalance/accountId={accountNumber}", 
    		method = RequestMethod.GET)
	public ResponseEntity<?> getAccountBalance(
    	   @RequestHeader Map<String, String> headerMap, 
    	   @PathVariable("accountNumber") String accountNumber) throws JsonProcessingException, IOException{
    	   
    	   MockAccountBalance accountBalance = new MockAccountBalance();
    
    	   if(accountNumber.equalsIgnoreCase("919999381925")) {
    		   accountBalance.setAccountNumber(accountNumber);    	   
    		   return new ResponseEntity<Object>(accountBalance.postitiveResponse(), HttpStatus.OK);
    	   } else if((accountNumber.length() < 12) | (accountNumber.length() > 12)) {
    		   return new ResponseEntity<Object>(accountBalance.negativeResponse(), HttpStatus.BAD_REQUEST);
    	   } else if (!accountNumber.matches("[0-9]+")){
    		   return new ResponseEntity<Object>(accountBalance.negativeResponse(), HttpStatus.BAD_REQUEST);
    	   } else {		   
    		
    		   BankHttpRequestHeaders headers = new BankHttpRequestHeaders(headerMap);
    		   String url = "https://secure-origin-ite.paytmbank.com/transaction/ext/v1/account-balance?accountIds="  + accountNumber; 
    		
    		   BankHttpRequest request = new BankHttpRequest(url, HttpMethod.GET, headers, new BankHttpRequestBody());	   
    		   RequestEntity<MultiValueMap<String, Object>> entity = new RequestEntity<MultiValueMap<String, Object>>(
    				   request.getRequestBody().getRequestBody(),request.getRequestHeaders().getRequestHeaders(), 
    				   request.getMethod(), URI.create(request.getUrl()));   
    		   
    		   try{
    			   entityResponse = restTemplate.exchange(entity, JsonNode.class);
    			   status = entityResponse.getStatusCode();
    			   response = entityResponse.getBody();	
    			   } catch (HttpStatusCodeException e) {
    				   status = e.getStatusCode();
    				   response = mapper.readTree(e.getResponseBodyAsString());     			
    				   }  		
    		   
    		   return new ResponseEntity<Object>(response, status);
    		   
    	   }
       }

}
